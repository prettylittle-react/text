import React from 'react';
import PropTypes from 'prop-types';

import Editable from 'editable';

import './text.scss';

/**
 * Text
 * @description [description]
 * @example
  <div id="Text"></div>
  <script>
	ReactDOM.render(React.createElement(Components.Text, {
		content			: `Example x`,
		className		: 'foo'
	}), document.getElementById("Text"));
  </script>
 */
const Text = props => {
	return <Editable {...props} baseClass="text" />;
};

Text.defaultProps = {
	...Editable.defaultProps,
	...{
		tag: 'span'
	}
};

Text.propTypes = {
	...Editable.propTypes,
	...{}
};

export default Text;
